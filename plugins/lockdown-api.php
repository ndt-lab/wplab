<?php
/**
 * Lockdown API
 *
 * @wordpress-plugin
 * Plugin Name:   Lockdown API
 * Description:   A sample demonstrate on how to prevent public query data through Rest API
 * Version:       1.0.0
 * Author:        ngdangtu
 * Author URI:    https://ngdangtu.com
 */

// https://stackoverflow.com/questions/43727060/wp-rest-api-set-permission-for-existing-endpoints
// https://wordpress.stackexchange.com/a/414035

function extract_path(): string
{
    $component = parse_url($_SERVER['REQUEST_URI']);

    if (str_starts_with($component['path'], '/wp-json')):
        return explode('/wp-json', $component['path'])[1];
    endif;

    $param = null;
    parse_str($component['query'], $param);
    return $param['rest_route'];
}

function error_auth(): WP_Error
{
    return new WP_Error('rest_forbidden', __('Authentication required.'), ['status' => 401]);
}

add_filter('rest_pre_dispatch', function($_, $server, $request)
{
    // $app_pass = $request->get_header('X-WP-Application-Password');
    $path = extract_path();
    $locklist = ['/wp/v2/cooking-recipes'];
    
    if (in_array($path, $locklist)):
        if (!isset($_SERVER['PHP_AUTH_USER'])) return error_auth();
        
        $user_id = wp_validate_application_password(false);
        if (!$user_id) return error_auth();
    endif;

    return null;
}, 69, 3);
<?php

function t($key): string { return __($key, 'v0id'); }

add_action('after_setup_theme', function()
{
    add_theme_support('title-tag');
});

add_action('customize_register', function($wp)
{
    $wp->remove_section('static_front_page');

    $wp->add_setting('cover', [
        'default'           => null,
        'sanitize_callback' => fn(string $url): int => attachment_url_to_postid($url),
    ]);

    $wp->add_setting('copyright', [
        'type'              => 'theme_mod',
        'default'           => t('The Void theme'),
        'sanitize_callback' => 'wp_kses_post',
    ]);

    $wp->add_panel('theme_option', [
        'title'      => t('Theme Options'),
        'capability' => 'edit_theme_options',
        'priority'   => 21,
        'panel'      => '',
    ]);

    $wp->add_section('main_content', [
        'title' => t('Main Content'),
        'panel' => 'theme_option',
    ]);

    $wp->add_control(new WP_Customize_Cropped_Image_Control(
        $wp, 
        'cover',
        [
            'label'       => t('Upload Cover'),
            'section'     => 'main_content',
            'settings'    => 'cover',
            'flex_width'  =>true, 
            'flex_height' =>true,
            'priority'    => '10',
        ]
    ));

    $wp->add_control('copyright', [
        'label'    => t('Copyright'),
        'type'     => 'textarea',
        'section'  => 'main_content',
        'settings' => 'copyright',
        'priority' => '90',
	]);
});
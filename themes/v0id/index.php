<?php use function \get_theme_mod as opt; ?>

<!DOCTYPE html>
<html lang='<?= esc_attr(get_bloginfo('language')) ?>'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <?php do_action('wp_head') ?>
    <style>
        :root {
            --w-container: 70rem;
            --w-edge: 1.6rem;
            --bdrs: 1rem;
            --f-size: 1.4rem;

            --c-tx: #111;
            --c-bg: #eeeeee;
            --c-border: #ddd;
            --c-bg-bright: #fafafa;
            --c-bg-dim: #f2f2f2;
        }
        @media screen and (min-width: 800px) {
            :root {
                --w-container: 120rem;
                --w-edge: 3.2rem;
                --bdrs: .5rem;
                --f-size: 1.8rem;
            }            
        }
        <?php require_once get_stylesheet_directory().'/inc.css' ?>
    </style>
</head>
<body>
    <main class='container'>
        <figure>
            <?= wp_get_attachment_image(opt('cover'), 'full', false) ?>
            <figcaption>
                <hgroup>
                    <h1>
                        <a href='<?= esc_url(home_url('/')) ?>'>
                            <?= t(get_bloginfo('name')) ?>
                        </a>
                    </h1>
                    <p><?= t(get_bloginfo('description')) ?></p>
                </hgroup>
            </figcaption>
        </figure>
    </main>
    <footer>
        <div class='container'>
            <small><?= t(opt('copyright')) ?></small>
        </div>
    </footer>
</body>
</html>
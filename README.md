# WordPress Laboratory

Dummy content to test WordPress API and other features.

## The Void

This theme is intentionally empty. It was made for Headless WordPress so it is for you if all you've ever wanted to do is serve WP API.

